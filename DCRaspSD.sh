#!/bin/bash
########################################################################
#                          by Flexkid                                  #
########################################################################
# This program is free software; you can redistribute it and/or modify #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation; either version 2 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# This program is distributed in the hope that it will be useful,      #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with this program; if not, write to the Free Software          #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,           #
# MA 02110-1301, USA.                                                  #
############################# DISCALIMER ###############################
########################################################################
############################# DEPENDENCE ###############################
#	Pipe Viewer                                                  	   #
# 	HomeBrew: Run "brew install pv" to get the latest version.  	   #
#	MacPorts: Run "port install pv" to get the latest version.     	   #
#	Or install the Rudix pv port (a simple package installer)      	   #
########################################################################
########################################################################




echo "Create restore and make the backup  of raspberry sd"


a="3"

function backup { 
echo "Insert sd card"
diskutil list
echo "Write the name of your peripheral (for example disk1)"
read -r perif
echo "Write the name of copy"
read -r copia
echo
echo "Please be patient"
sudo pv -tpreab  /dev/$perif | dd bs=8388608 of=~/Desktop/$copia.dmg

} 

function restore {
echo "Insert sd card"
diskutil list
echo "Write the name of your peripheral (for example disk1)"
read -r perif
echo
diskutil unmountDisk /dev/$perif
echo "Format sd card"
sudo newfs_msdos -F 16 /dev/$perif
echo
echo "Restore from a Cloned Image or create a new one"
echo "Move the backup or the original image on your desktop"
echo "Write the name of backup or the original image  name (without extension)"
read -r copia
echo "Please be patient"
dd if=~/Desktop/$copia.dmg bs=1m | pv | sudo dd of=/dev/r$perif bs=1m
sudo diskutil eject /dev/r$perif

}



while [ $a -ne 0 ]
do
echo
echo "#############################"
echo "#    Welcome  $USER       #"
echo "#############################"
echo "Disconnect all peripherals"
echo "0) Select Zero for exit"
echo "1) Select one if you wish to make sd backup"
echo "2) Select two  if you wish to restore sd card or create new one"
read -r b
a=$b
if (( b == 1 )) ; then			
	backup
	echo "Finish"
	echo
elif (( b == 2)) ; then
	 restore
	 echo "Finish"
	 echo    
	     	    	   
elif [[ $a == "0" ]] ; then
	echo "#############################"
	echo "#       Bye  $USER        #"
	echo "#############################"
	echo
	exit	    	    	    	    	    	        	    	        	    	        	    	        	    	    
	        	     	    	                	     	    	        	    	        	    	    
else
	echo "scelta non valida"
fi   
done


